"""Caching engine for python"""
from .cache import *  # noqa: F403, F401

__version__ = "1.3 beta"
