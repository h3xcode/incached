flake8==3.8.2
mypy==0.780
pylint==2.5.2
deprecation==2.1.0
pycryptodome==3.9.7